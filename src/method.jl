include("random_search.jl")
include("coord_search.jl")

function solveProblem(pID)
  pID = Cint(pID)
  safeSetProblem(pID)

  bud = safeGetBudget(pID)
  dim = safeGetDimension(pID)
  evals = safeGetEvaluations(pID)

  println((bud,dim,evals))

  random_budget = div(bud,100)
  best, best_f = random_search(pID, dim, random_budget-evals)
  best, best_f = coord_search(pID, best, best_f, bud-evals)

  println("best = ", best)
  println("best_f = $best_f")
end
