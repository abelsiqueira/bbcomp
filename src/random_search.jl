# Randomly selects points.
# Returns the one with the lowest fval
function random_search (pID, dim::Integer, kmax::Integer)
  k = 0
  best_f = Inf
  best = zeros(dim)
  while k < kmax
    x = rand(dim)
    f = safeEvaluate(pID, x)
    k += 1
    if f < best_f
      copy!(best, x)
      best_f = f
    end
  end
  return best, best_f
end
