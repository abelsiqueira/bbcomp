# Coordinate search.
# Starts with δ = 0.1 from a given point, and stops when δ < tol,
# or the budget expires.
# The default tol is 0.

function feasible(x::Vector)
  for i = 1:length(x)
    if x[i] < 0.0 || x[i] > 1.0
      return false
    end
  end
  return true
end

function coord_search (pID, best::Vector, best_f::Real, kmax::Integer,
    tol::Real = 0.0)
  δ = 0.1

  dim = length(best)
  y = zeros(dim)
  k = 0
  while δ > tol && k < kmax
    best_i = 0
    copy!(y, best)

    for i = 1:dim
      for s = [-1, 1]
        y[i] = best[i] + s*δ
        if feasible(y)
          f = safeEvaluate(pID, y)
          k += 1
        else
          f = Inf
        end
        if f < best_f
          best_f = f
          best_i = s*i
        end
        if k == kmax
          break
        end
      end
      if k == kmax
        break
      end
    end
    if best_i != 0
      i = abs(best_i)
      s = sign(best_i)
      best[i] += s*δ
    else
      δ *= 0.5
    end
  end

  return best, best_f
end
